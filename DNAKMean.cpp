#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits.h>
#include <float.h> 
#include <math.h>
#include "DistanceCalculator.h"
#include "DNAKMean.h"

int GetInfo(Distance* _pFirst)
{
	double min = DBL_MAX;
	Distance *current = _pFirst;
	int minIndex = -1;
	int index = -1;
	while (current != NULL)
	{
		index++;
		if(min > current -> distance)
		{
			min = current -> distance;
			//pMin = current;
			minIndex = index;
		}
		current = current -> pNext;
	}
	return minIndex;
}

double doubleRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

vector<DNAGCCenter> Random_GC_Center(DNACollection* collection) {
	
	int totalReads = arrTotalReads[fileIndex];
	vector<DNAGCCenter> pCenters(totalSpecies);
	vector<int> positions(totalSpecies);

	for (int i = 0; i < totalSpecies; ++i)
	{
		srand(time(NULL));
		positions[i] = rand() % totalReads + 1;  //number between 1 and totalReads

		//make sure no duplicate center
		for (int j = 0; j < i; ++j)
		{
			if (positions[i] == positions[j])
			{
				--i;
				break;
			}
		}
	}

	cout <<"HERE"<<endl;
	DNASequence* currentSequence = collection -> pFirst;
	for (int i = 0; i < totalReads; ++i)
	{
		for (int j = 0; j < totalSpecies; ++j)
		{
			if (positions[j] == i + 1)
			{
				pCenters[j] = DNAGCCenter(currentSequence -> _gcValue);
				break;
			}
		}

		currentSequence = currentSequence -> pNext;
	}

	return pCenters;
}

vector<DNACenter> Random_Center(DNACollection* collection){
	int totalReads = arrTotalReads[fileIndex];
	vector<DNACenter> pCenters(totalSpecies);
	vector<int> positions(totalSpecies);

	for (int i = 0; i < totalSpecies; ++i)
	{
		srand(time(NULL));
		positions[i] = rand() % totalReads + 1;  //number between 1 and totalReads

		//make sure no duplicate center
		for (int j = 0; j < i; ++j)
		{
			if (positions[i] == positions[j])
			{
				--i;
				break;
			}
		}
	}

	// positions[0] = 1;
	// positions[1] = totalReads;

	DNASequence* currentSequence = collection -> pFirst;
	for (int i = 0; i < totalReads; ++i)
	{
		for (int j = 0; j < totalSpecies; ++j)
		{
			if (positions[j] == i + 1)
			{
				pCenters[j] = DNACenter(*(currentSequence));
				break;
			}
		}
		currentSequence = currentSequence -> pNext;
	}

	return pCenters;
}

void DNA_Kmean(DNACollection* collection) {
	int totalPoints = 0; //number of elements
	totalPoints = collection->numOfSequence;

	// DNACenter pCenters[totalSpecies] = { DNACenter(*(collection->pFirst)), DNACenter(*(collection->pLast)) };

	vector<DNACenter> pCenters = Random_Center(collection);
	
	bool endLoop = true;
	int countLoop = 0;
	do
	{
		countLoop++;
		cout << "Loop: " << countLoop <<" - ";

		vector<DNACenter> pC(totalSpecies);
		for (int i = 0; i < totalSpecies; i++)
		{
			pC[i] = pCenters[i];
			pC[i].pFrist = NULL;
		}

		DNASequence* currentSequence = collection -> pFirst;
		for (int j = 0; j < totalPoints; j++) {

			//calculate distance form all points to all centers
			DistanceList distanceList = DistanceList(totalSpecies);
			Distance *currentDis = distanceList.pFirst;
			int centerIndex = 0;
			while (currentDis != NULL)
			{
				double distance = calculateDistance(
					distanceFuncs, 
					currentSequence -> _kmerVector, 
					pC[centerIndex].point._kmerVector,
					currentSequence -> vectorMedian, 
					pC[centerIndex].point.vectorMedian
				);

				currentDis -> distance = distance;
				currentDis = currentDis->pNext;
				centerIndex += 1;
			}
			int nearestCenterIndex = GetInfo(distanceList.pFirst);

			pC[nearestCenterIndex].Add(currentSequence, nearestCenterIndex);

			currentSequence = currentSequence -> pCollectionNext;
		}

		//update centers
		endLoop = true;
		for (int j = 0; j < totalSpecies; j++)
		{
			pC[j].UpdateCenter();
			for(int i = 0; i < V; i++) {
				
				if(pC[j].point._kmerVector[i] != pCenters[j].point._kmerVector[i]) {
					pCenters[j].point._kmerVector[i] = pC[j].point._kmerVector[i];
					endLoop = false;
				}
			}			
		}
		//update cluster
		if(!endLoop)
		{
			for (int j = 0; j < totalSpecies; j++)
			{
				pC[j].UpdateCluster(&pCenters[j]);
			}
		}
	} while (!endLoop && countLoop < maxKmeanLoop);

	cout<<"\n";
}

void OutlierClutering(DNACollection* collection) {

	cout<<"STEP 1"<<endl;
	// Step 1 init kmer vectors
	vector<vector<double> > kmerVectors(totalSpecies, vector<double>(V, 0));
	vector<int> clusterNumberOfSeq(totalSpecies, 0);

	cout<<"STEP 2"<<endl;
	// step 2, calculate clusters kmer vector
	DNASequence* currentSequence = collection -> pFirst;
	for (int i = 0; i < collection -> numOfSequence; ++i)
	{
		int clusterId = currentSequence -> clusterId;
		if(clusterId != -1) {
			for (int j = 0; j < V; ++j)
			{
				kmerVectors[clusterId][j] += currentSequence -> _kmerVector[j];
			}
			++clusterNumberOfSeq[clusterId];
		}

		currentSequence = currentSequence -> pCollectionNext;
	}

	for (int i = 0; i < totalSpecies; ++i)
	{
		for (int j = 0; j < V; ++j)
		{
			kmerVectors[i][j] /= clusterNumberOfSeq[i];
		}
	}

	cout<<"STEP 3"<<endl;
	currentSequence = collection -> pFirst;
	// step 3, assign each unclustered read to nearest cluster
	for (int i = 0; i < collection -> numOfSequence; ++i)
	{
		int clusterId = currentSequence -> clusterId;
		if(clusterId == -1) {
			clusterId = nearesetKmerCenter(distanceFuncs, 
				kmerVectors, currentSequence -> _kmerVector);
			cout<<"Oulier: "<<currentSequence -> _id<<endl;
		}

		currentSequence = currentSequence -> pCollectionNext;
	}
}

void OutlierDetect(DNACollection* collection, vector<DNAGCCenter> pCenters) {
	for (int i = 0; i < pCenters.size(); ++i)
	{
		pCenters[i].CalculateStandardError();
		pCenters[i].maxStandardError = 2 * pCenters[i].standardError;
	}

	for (int i = 0; i < pCenters.size(); ++i)
	{
		cout << "Index: " << i << endl;
		cout << "standardError: " << pCenters[i].standardError << endl;
	}


	DNASequence* currentSequence = collection -> pFirst;
	for (int i = 0; i < collection -> numOfSequence; ++i)
	{

		double distance = calculateGCDistance(
			distanceFuncs, 
			currentSequence -> _gcValue, 
			pCenters[currentSequence -> gcClusterId].gcValue
		);
		cout << "ID : " << i << endl;
		cout << "GC : " << currentSequence -> _gcValue << endl;
		cout << "center GC : " << pCenters[currentSequence -> gcClusterId].gcValue << endl;
		cout << "standardError: " << pCenters[currentSequence -> gcClusterId].maxStandardError << endl;
		cout << "distance : " << distance << endl;
		
		if (distance < pCenters[currentSequence -> gcClusterId].maxStandardError) {
			currentSequence -> clusterId = currentSequence -> gcClusterId;
		}
		currentSequence = currentSequence -> pCollectionNext;
	}

	cout << "DONE OUTLIER" << endl;
}

void GC_Kmean_Loop(DNACollection* collection) {
	int totalPoints = collection->numOfSequence;

	// DNACenter pCenters[totalSpecies] = { DNACenter(*(collection->pFirst)), DNACenter(*(collection->pLast)) };

	vector<DNAGCCenter> pCenters = Random_GC_Center(collection);

	// for (int i = 0; i < 2; ++i)
	// {
	// 	cout<< "Center: " << i <<"\n";
	// 	cout<< "gc: " << pCenters[i].gcValue<<"\n";
	// }

	// DNASequence* cSeq = collection -> pFirst;
	// for (int i = 0; i < 11; ++i)
	// {
	// 	cout<< "SEQ: " << i <<"\n";
	// 	cout<< "gc: " << cSeq -> _gcValue<<"\n";
	// 	cSeq = cSeq -> pNext;
	// }

	bool endLoop = true;
	int countLoop = 0;
	do
	{
		countLoop++;
		cout << "Loop: " << countLoop <<" - ";

		vector<DNAGCCenter> pC(totalSpecies);
		for (int i = 0; i < totalSpecies; i++)
		{
			pC[i] = pCenters[i];
			pC[i].pFrist = NULL;
		}

		DNASequence* currentSequence = collection -> pFirst;
		for (int j = 0; j < totalPoints; j++) {

			//calculate distance form all points to all centers
			DistanceList distanceList = DistanceList(totalSpecies);
			Distance *currentDis = distanceList.pFirst;
			int centerIndex = 0;
			while (currentDis != NULL)
			{
				double distance = calculateGCDistance(
					distanceFuncs, 
					currentSequence -> _gcValue, 
					pC[centerIndex].gcValue
				);

				// cout<< "SEQ: " << currentSequence -> _id <<"\n";
				// cout<< "Center : " << centerIndex <<"\n";
				// cout<< "Center gcValue: " << pC[centerIndex].gcValue <<"\n";
				// cout<< "Dis : " << distance <<"\n";

				currentDis -> distance = distance;
				currentDis = currentDis->pNext;
				++centerIndex;

			}
			int nearestCenterIndex = GetInfo(distanceList.pFirst);

			pC[nearestCenterIndex].Add(currentSequence, nearestCenterIndex);

			currentSequence = currentSequence -> pCollectionNext;
		}

		//update centers
		endLoop = true;
		for (int j = 0; j < totalSpecies; j++)
		{
			pC[j].UpdateCenter();
			for(int i = 0; i < V; i++) {
				
				if(pC[j].gcValue != pCenters[j].gcValue) {
					pCenters[j].gcValue = pC[j].gcValue;
					endLoop = false;
				}
			}			
		}
		//update cluster
		if(!endLoop)
		{
			for (int j = 0; j < totalSpecies; j++)
			{
				pC[j].UpdateCluster(&pCenters[j]);
			}
		}
	} while (!endLoop && countLoop < maxKmeanLoop);

	cout<<"Hey done!\n";

	OutlierDetect(collection, pCenters);
}