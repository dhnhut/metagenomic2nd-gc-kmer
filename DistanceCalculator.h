#ifndef __DISTANCECALCULATOR_H__
#define __DISTANCECALCULATOR_H__

#include <cstdlib>
#include "Config.h"
#include <vector>

double calculateDistance(string distanceName, double stVec[], double ndVec[],
	double stMedian, double ndMedian);

double calculateGCDistance(string distanceName, double firstVal, double secondVal);

double euclideanDistance(double* stVec, double* ndVec);

double pearsonDistance(double* stVec, double* ndVec);

double findMedian(double arr[], int left, int right);

int nearesetKmerCenter(string distanceName,
	vector<vector<double> > kmerVector, double readVec[]);

#endif