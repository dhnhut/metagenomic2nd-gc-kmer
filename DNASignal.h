#ifndef __DNASignal_H__
#define __DNASignal_H__ 

#include "global.h"
#include "Config.h"

using namespace std;

void GenerateKMerVector(double *_vector, string _DNAString, string _AdditionalString);

double GenerateGCContentVector(string _DNAString, string _AdditionalString);

void GenerateFOMVector(double *_vector, string _DNAString, string _AdditionalString);

void GenerateSOMVector(double *_vector, string _DNAString, string _AdditionalString);

#endif