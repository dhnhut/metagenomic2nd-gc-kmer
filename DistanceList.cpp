#include "DistanceList.h"
#include <iostream>

using namespace std;

DistanceList::DistanceList(void)
{

}

DistanceList::DistanceList(int _numOfElement) {
	numOfElement = _numOfElement;
	pFirst = NULL;
	pLast = NULL;
	pMin = NULL;
	CreateList();
}

DistanceList::~DistanceList(void)
{
}

void DistanceList::CreateList() {
	if(numOfElement == 0)
		return;
	//init first element for list
	pFirst = new Distance();
	pMin = pFirst;
	pLast = pFirst;

	//init other elements
	for (int i = 0; i < numOfElement - 1; i++)
	{
		Distance* dis = new Distance();
		pLast->pNext = dis;
		pLast = dis;
		pLast->pNext = NULL;
	}
}