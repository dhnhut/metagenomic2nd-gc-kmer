#ifndef __DNAGCCenter_H__
#define __DNAGCCenter_H__

#include "global.h"
#include "DNASequence.h"

class DNAGCCenter
{
public:
	DNAGCCenter(void);
	DNAGCCenter(double gcValue);
	~DNAGCCenter(void);

	double gcValue;
	DNASequence *pFrist, *pLast;

	double totalGCVal;
	int totalElement;
	double mean;
	double variance;
	double standardError;
	double maxStandardError;

	void Add(DNASequence *p, int gcClusterId);
	void UpdateCenter();
	void UpdateCluster(DNAGCCenter *pCenter);
	void CalculateStandardError();
	void ExportCluster();
};

#endif