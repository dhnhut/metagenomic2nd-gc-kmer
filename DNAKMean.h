#ifndef __DNAKMean_H__
#define __DNAKMean_H__

#include "global.h"
#include "DNACenter.h"
#include "DNAGCCenter.h"
#include "DNACollection.h"
#include "Distance.h"
#include "DistanceList.h"
#include "DNAKMean.h"

void DNA_Kmean(DNACollection* collection);

void GC_Kmean_Loop(DNACollection* collection);

void OutlierClutering(DNACollection* collection);

#endif