#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits.h>
#include <math.h> 
#include <float.h> 
#include <fstream>
#include <sstream>
#include "DNAKMedoids.h"

struct SwapInfo
{
	double minSwapCost;
	int mediodIndex;
	int sequenceIndex;
};

vector<int> Random_Medoids(DNACollection* collection){
	int totalReads = arrTotalReads[fileIndex];
	vector<int> pMedoids(totalSpecies);

	for (int i = 0; i < totalSpecies; ++i)
	{
		srand(time(NULL));
		pMedoids[i] = rand() % totalReads;  //number between 1 and totalReads
		//make sure no duplicate center
		for (int j = 0; j < i; ++j)
		{
			if (pMedoids[i] == pMedoids[j])
			{
				--i;
				break;
			}
		}
	}
	return pMedoids;
}

double CalculateDistance(DNASequence* first, DNASequence* second) {
	double distance = 0;

	for (int i = 0; i < V; i++)
	{
		double a = first -> _kmerVector[i];
		double b = second -> _kmerVector[i];
		double dis = a - b;
		distance += dis * dis;
	}

	return distance;
}

double FindDistance(int first, int second, vector< vector<double> > *matrix) {
	vector< vector<double> > &vr = *matrix; //Create a reference
	if(first < second)
		return (*matrix)[first][second];
	else
		return (*matrix)[second][first];
}

DNASequence* GetMedoidsByIndex(DNACollection* collection, int pMedoid){
	int totalReads = collection -> numOfSequence;

	// get current medoid sequences
	DNASequence* currentSequence = collection -> pFirst;
	for (int i = 0; i < totalReads; ++i)
	{
		// Check if current sequence is medoid or not
		if (i == pMedoid)
		{
			// if sequence is mediod, 
			return currentSequence;
		}

		currentSequence = currentSequence -> pNext;
	}

	return NULL;
}

double AssignToMedoids(DNACollection* collection, vector<int> pMedoids, vector< vector<double> > *matrix) {
	int totalReads = collection->numOfSequence;
	// DNASequence* medoids[totalSpecies];
	// for (int i = 0; i < totalSpecies; ++i)
	// {
	// 	medoids[i] = GetMedoidsByIndex(collection, pMedoids[i]);
	// }

	double totalCost = 0;

	DNASequence* currentSequence = collection -> pFirst;
	for (int i = 0; i < totalReads; ++i)
	{
		// Check if current sequence is medoid or not
		for (int j = 0; j < totalSpecies; ++j)
		{
			if (i == pMedoids[j])
			{
				// if sequence is mediod, 
				currentSequence -> isMedoid = true;;
				currentSequence -> gcClusterId = j;
				break;
			}
		}

		// if current sequence is not medoid,
		// calculate distance from it to all medoids,
		// then assign it to closest medoid cluster
		if(!currentSequence -> isMedoid){
			double minDistance = DBL_MAX;
			int minIndex = -1;
			for (int p = 0; p < totalSpecies; ++p)
			{
				// double distance = CalculateDistance(medoids[p], currentSequence);
				double distance = FindDistance(pMedoids[p], i, matrix);
				if (distance < minDistance){
					minDistance = distance;
					minIndex = p;
				}
			}
			totalCost += minDistance;
			currentSequence -> gcClusterId = minIndex;
		}
		currentSequence = currentSequence -> pNext;
	}
	return totalCost;
}

SwapInfo SwapMedoids(DNACollection* collection, vector<int> pMedoids, vector< vector<double> > *matrix) {

	SwapInfo swapInfo;
	swapInfo.minSwapCost = DBL_MAX;
	swapInfo.mediodIndex = 0;
	swapInfo.sequenceIndex = 0;

	vector<DNASequence> medoids(totalSpecies);
	for (int i = 0; i < totalSpecies; ++i)
	{
		medoids[i] = *GetMedoidsByIndex(collection, pMedoids[i]);
	}

	int totalReads = collection->numOfSequence;
	// for each medoid
	for (int m = 0; m < pMedoids.size(); ++m)
	{
		DNASequence* currentSequence = collection -> pFirst;
		for (int non_m = 0; non_m < totalReads; ++non_m)
		{
			if(!currentSequence -> isMedoid){
				// swap current medoid
				vector<int> swapMedoids = pMedoids;
				swapMedoids[m] = non_m;
				DNASequence* swapMed = GetMedoidsByIndex(collection, non_m);
				swapMed -> isMedoid = true;
				medoids[m].isMedoid = false;

				// caculate total distance for this swapping
				double dis = AssignToMedoids(collection, swapMedoids, matrix);
				if(dis < swapInfo.minSwapCost){
					swapInfo.minSwapCost = dis;
					swapInfo.mediodIndex = m;
					swapInfo.sequenceIndex = non_m;
				}

				// revert swapping
				medoids[m].isMedoid = true;
				swapMed -> isMedoid = false;
			}
			currentSequence = currentSequence -> pNext;
		}
	}
	return swapInfo;
}

void DNA_KMedoids(DNACollection* collection) {
	
	vector< vector<double> > matrix = ReadDistanceMatrix();

	double minTotalCost = 0;

	// step 1: random choice medoids
	vector<int> pMedoids = Random_Medoids(collection);
	
	bool isChanged = true;
	int loop = 0;

	// step 2: assign collection to medoids
	minTotalCost = AssignToMedoids(collection, pMedoids, &matrix);
	do
	{
		// step 3: swap each medoid to non-medoid
		SwapInfo swapInfo = SwapMedoids(collection, pMedoids, &matrix);
		// step 4: Select lowest cost
		cout<<"\nLoop: "<< loop << " - cost: " << minTotalCost << " - swap: " << swapInfo.minSwapCost <<"\n";
		if(swapInfo.minSwapCost < minTotalCost){
			// swap to lowest change
			DNASequence* current = GetMedoidsByIndex(collection, pMedoids[swapInfo.mediodIndex]);
			DNASequence* swap = GetMedoidsByIndex(collection, swapInfo.sequenceIndex);

			current -> gcClusterId = swap -> gcClusterId;
			current -> isMedoid = false;
			swap -> isMedoid = true;

			minTotalCost = swapInfo.minSwapCost;
			pMedoids[swapInfo.mediodIndex] = swapInfo.sequenceIndex;
		}
		else {
			isChanged = false;
		}
		loop++;
	// step 5: check if medoids are changed or not to go back step 2 or exits
	} while (isChanged);
}

void GenDistanceMatrix(DNACollection* collection){
	string fullFileName = ResultsFolderName + fileName + signalNames[signalId] + "distances.matrix";
	const char *cstr = fullFileName.c_str();
	ofstream myfile;
	myfile.open (cstr, std::ios::out);

	int totalReads = arrTotalReads[fileIndex];
	// int totalReads = 5;
	DNASequence* currentRowSeq = collection -> pFirst;
	for (int row = 0; row < totalReads; ++row)
	{
		DNASequence* currentColSeq = collection -> pFirst;

		for (int visitedRow = 0; visitedRow <= row; ++visitedRow)
		{
			currentColSeq = currentColSeq -> pNext;
			// matrix[row][visitedRow] = 0;
			myfile << "0,";
		}

		for (int col = row + 1; col < totalReads; ++col)
		{
			double distance = CalculateDistance(currentRowSeq, currentColSeq);
			// matrix[row][col] = distance;
			ostringstream ss;
			ss << distance;
			myfile << ss.str() + ",";
			currentColSeq = currentColSeq -> pNext;
		}
		myfile << "\n";
		currentRowSeq = currentRowSeq -> pNext;
	}

	myfile.close();
}

vector< vector<double> > ReadDistanceMatrix(){
	int totalReads = arrTotalReads[fileIndex];

	vector< vector<double> > matrix(totalReads, vector<double>(totalReads));
	string fullFileName = ResultsFolderName + fileName + signalNames[signalId] + "distances.matrix";
	const char *cstr = fullFileName.c_str();

	ifstream myFile(cstr);
	if(!myFile)
	{
		cout << "Cannot open input Matrix file: " << fileName;
		return matrix;
	}

	for (int row = 0; row < totalReads; ++row)
	{
		string myBuf;
		getline(myFile, myBuf); //get a string in a new line
		if(!myBuf.empty())
		{
			istringstream iss(myBuf);
			vector<string> tokens;
			string token;

			int i = 0;
			while(std::getline(iss, token, ',')) {
				matrix[row][i] = atof(token.c_str());
				i++;
			}
		}
	}

	return matrix;
	myFile.close();
}