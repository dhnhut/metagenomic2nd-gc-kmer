#include "DNAGCCenter.h"
#include <math.h> 
#include <iostream>

DNAGCCenter::DNAGCCenter(void) {
}

DNAGCCenter::~DNAGCCenter(void) {
}

DNAGCCenter::DNAGCCenter(double _gcValue) {
	gcValue = _gcValue;
	pLast = NULL;
	pFrist = NULL;

	mean = 0;
	variance = 0;
	standardError = 0;
	maxStandardError = 0;
}

void DNAGCCenter::Add(DNASequence *p, int gcClusterId) {
	p -> gcClusterId = gcClusterId;
	if(pFrist == NULL) {
		pFrist = p;
		pLast = p;
	} else {
		pLast -> pNewNext = p;
		pLast = p;
	}
}

void DNAGCCenter::UpdateCenter() {
	if(pFrist == NULL)
		return;
	int count = 0;
	double sum = 0;
	DNASequence *pTmp = pFrist;
	do {
		sum += pTmp -> _gcValue;
		pTmp = pTmp -> pNewNext;
		count++;
	} while (pTmp != NULL);

	gcValue = sum/count;
}

void DNAGCCenter::UpdateCluster(DNAGCCenter *pCenter) {
	pCenter -> pFrist = pFrist;
	pCenter -> pLast = pLast;

	if(pFrist == NULL)
		return;
	DNASequence *pTmp = pFrist;
	do {
		pTmp -> pNext = pTmp -> pNewNext;
		pTmp -> pNewNext = NULL;
		pTmp = pTmp -> pNext;
	} while (pTmp != NULL);
}

void DNAGCCenter::CalculateStandardError() {
	// cout << "Hello" << endl;

	DNASequence *current;
	current = pFrist;

	totalElement = 0;
	totalGCVal = 0;

	while (current != NULL)
	{
		cout << "GC value : " <<current -> _gcValue<< endl;
		totalGCVal += current -> _gcValue;
		totalElement++;
		current = current -> pNext;
	}


	mean = totalGCVal / totalElement;

	variance = 0;

	current = pFrist;
	while (current != NULL)
	{
		variance += pow(current -> _gcValue - mean, 2);
		current = current -> pNext;
	}

	standardError = sqrt(variance / totalElement);

	cout << "GC: " <<totalGCVal<< endl;
	cout << "Element: " << totalElement<<endl;
	cout << "Mean: " << mean<<endl;
	cout << "variance: " << variance<<endl;
	cout << "standardError: " << standardError<<endl;

	cout << "DONE" << endl;
}

void DNAGCCenter::ExportCluster()
{
	DNASequence *current;
	current = pFrist;
	printf("\nCLUSTER MEMBERS: ");
	int numOfElement = 0;
	while (current != NULL)
	{
		cout << current -> _id;
		//cout << current -> _id << ": " << current -> ExportVector();
		current = current -> pNext;
		numOfElement++;
		if(current != NULL)
			cout << " - ";
	}
	printf("\nTOTAL MEMBER: %d \n", numOfElement);
}
