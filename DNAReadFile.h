#ifndef __DNAReadFile_H__
#define __DNAReadFile_H__

#include <iostream>
#include <fstream>

#include "global.h"
#include "DNACollection.h"


using namespace std;
using std::cout;
using std::endl;

DNACollection ReadDNA();
DNACollection ReadVector();

#endif