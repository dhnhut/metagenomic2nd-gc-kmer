#ifndef __DNACenter_H__
#define __DNACenter_H__

#include "global.h"
#include "DNASequence.h"

class DNACenter
{
public:
	DNACenter(void);
	DNACenter(DNASequence _point);
	~DNACenter(void);

	DNASequence point;
	DNASequence *pFrist, *pLast;

	void Add(DNASequence *p, int gcClusterId);
	void UpdateCenter();
	void UpdateCluster(DNACenter *pCenter);
	void ExportCluster();
};

#endif