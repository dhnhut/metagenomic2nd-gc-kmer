#include <sstream>
#include <iterator>
#include <stdlib.h> 
#include <stdio.h>
#include "DNASequence.h"
#include "DistanceCalculator.h"

DNASequence::DNASequence(void)
{
	_DNAString = "";
	gcClusterId = -1;
	clusterId = -1;
	pNext = NULL;
	pNewNext = NULL;
	pCollectionNext = NULL;
	isMedoid = false;

	int size = sizeof(_kmerVector)/sizeof(*_kmerVector);
	for(int i = 0; i < size; i++)
	{
		_kmerVector[i] = 0.0;
	}
}

DNASequence::DNASequence(string _sid) 
{
	_DNAString = "";
	_id = _sid;
	gcClusterId = -1;
	clusterId = -1;
	pNext = NULL;
	pNewNext = NULL;
	pCollectionNext = NULL;
	isMedoid = false;

	int size = sizeof(_kmerVector)/sizeof(*_kmerVector);
	for(int i = 0; i < size; i++)
	{
		_kmerVector[i] = 0.0;
	}
}

DNASequence::~DNASequence(void)
{
}

//add subsequence to current DNA string
void DNASequence::Append(string subStr)
{
	_DNAString.append(subStr);
}

void DNASequence::GenerateAdditionalDNAString()
{
	_AdditionalDNAString = _DNAString;
	for (int i = 0; i < _DNAString.length(); i++)
	{
		switch (_DNAString[_DNAString.length() - i - 1])
		{
		case 'A':
			_AdditionalDNAString[i]  = 'T';
			break;
		case 'T':
			_AdditionalDNAString[i]  = 'A';
			break;
		case 'G':
			_AdditionalDNAString[i]  = 'C';
			break;
		case 'C':
			_AdditionalDNAString[i]  = 'G';
			break;
		}
		
	}
}

//export DNA string
string DNASequence::ExportDNA(void)
{
	return _DNAString;
}

//export DNA id
string DNASequence::Id()
{
	return _id;
}

void DNASequence::GenerateVector(int signal)
{
	GenerateAdditionalDNAString();

	_gcValue = GenerateGCContentVector(_DNAString, _AdditionalDNAString);
	GenerateKMerVector(_kmerVector, _DNAString, _AdditionalDNAString);
}

void DNASequence::InputVector(string _strVector)
{
	istringstream iss(_strVector);
	vector<string> tokens;
	copy(istream_iterator<string>(iss),
		istream_iterator<string>(),
		back_inserter<vector<string> >(tokens));
	_id = tokens[0];
	for (int i = 0; i < V; i++)
	{
		_kmerVector[i] = atof(tokens[i+1].c_str());
	}
}

string DNASequence::ExportVector() 
{
	string strVector = "";
	int size = sizeof(_kmerVector)/sizeof(*_kmerVector);

	for(int i = 0; i < size; i++)
	{
		char numstr[21]; // enough to hold all numbers up to 64-bits
		// _snprintf(numstr, 21, "%f", _kmerVector[i]); // windows
		snprintf(numstr, 21, "%f", _kmerVector[i]); // linux
		strVector.append(numstr);
		if(i + 1 < size)
		{
			strVector.append(" - ");
		}
	}
	return strVector;
}

double* DNASequence::Vector()
{
	return _kmerVector;
}